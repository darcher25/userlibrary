package com.example.UserLibrary;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;


public class UserDao {
    /**
     * Save User
     * @param user
     *
     */
    public void saveUser(UsersEntity user) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()){
            //start transaction
            transaction = session.beginTransaction();

            //save user object
            session.save(user);

            //commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Update User
     * @param user
     *
     */

    public void updateUser(UsersEntity user) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            //start a transaction
            transaction = session.beginTransaction();

            //update user object
            session.update(user);

            //commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    /**
     * Delete User
     * @param id
     *
     */
    public void deleteUser(int id) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            //start transaction
            transaction = session.beginTransaction();

            //delete user object
            UsersEntity user = session.get(UsersEntity.class, id);
            if (user != null) {
                session.delete(user);
                System.out.println("User is Deleted");
            }

            //commit transaction
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    /**
     * Get User by ID
     * @param id
     * @return
     *
     */
    public UsersEntity getUser(int id) {
        Transaction transaction = null;
        UsersEntity user = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            user = session.get(UsersEntity.class, id);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return user;
    }

    /** Get all Users
     * @return
     *
     */
    @SuppressWarnings("unchecked")
    public java.util.List<UsersEntity> getAllUser() {
        Transaction transaction = null;
        List<UsersEntity> listOfUser = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            listOfUser = session.createQuery("from UsersEntity ").getResultList();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return listOfUser;
    }
}
