
<title>ELibrary User Management</title>
</head>
<body>
<div style="text-align: center;">
    <h1><%= "User Management" %></h1>
    <h2>
        <a href="new">Add New User</a>
        &nbsp;&nbsp;&nbsp;
        <a href="list">List All Users</a>
    </h2>
</div>
<br/>
<div style="text-align: center;">
    <form div style="text-align: center;">
        <c:if test="${user != null}">
        <form action="update" method="post"></form>
        </c:if>
        <c:if test="${user == null}">
            <form action="insert" method="post"></form>
        </c:if>
        <table border="1" cellpadding="5">
            <caption>
                <h2>
                    <c:if test="${user != null}">
                        Edit User
                    </c:if>
                    <c:if test="${user == null}">
                        Add New User
                    </c:if>
                </h2>
            </caption>
            <input type="hidden" name="id" value="c:out {user.id}">
            <tr>
                <th>User Name: </th>
                <td>
                    <input type="text" name="name" size="45">
                </td>
            </tr>
            <tr>
                <th>User Email: </th>
                <td>
                    <input type="text" name="email" size="45">
                </td>
            </tr>
            <tr>
                <th>Country: </th>
                <td>
                    <input type="text" name="country" size="15">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Save" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>